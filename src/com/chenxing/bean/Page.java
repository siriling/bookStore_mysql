package com.chenxing.bean;

import java.util.List;

/**
 * Date:2020/10/14
 * Author:Ling
 * Description:
 */
public class Page <T>{

    //已知条件：直接可以获取的值

    //从浏览器所获取的当前页的页码
    private String pageNoStr;
    //每页显示的条数
    private Integer pageSize=4;
    //总记录数
    private Integer totalCount;

    //未知条件：根据已知条件获取的值

    //转换为Integer类型的当前页的页码pageNo
    private Integer pageNo;
    //总页数
    private Integer totalPage;
    //分页数据
    private List<T> list;
    //动态获取的当前的请求路径
    private String path;

    public void setPageNoStr(String pageNoStr) {
        this.pageNoStr = pageNoStr;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getPageNo() {
        pageNo=1;
        try {
            //能转换就转换转换不了也没事，pageNo有默认值
            pageNo=Integer.parseInt(pageNoStr);
        } catch (NumberFormatException e) {
//            e.printStackTrace();
        }
        //若pageNo大于总页数的，默认访问最后一页
        if(pageNo>=getTotalPage())
        {
            pageNo=getTotalPage();
        }
        //若pageNo小于等于0，默认访问第一页
        if(pageNo<=0)
        {
            pageNo=1;
        }
        return pageNo;
    }

    public Integer getTotalPage() {
        if(totalCount%pageSize==0)
        {
            totalPage=totalCount/pageSize;
        }
        else
        {
            totalPage=totalCount/pageSize+1;
        }
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "Page{" +
                "pageNoStr='" + pageNoStr + '\'' +
                ", pageSize=" + pageSize +
                ", totalCount=" + totalCount +
                ", pageNo=" + pageNo +
                ", totalPage=" + totalPage +
                ", list=" + list +
                ", path='" + path + '\'' +
                '}';
    }
}
