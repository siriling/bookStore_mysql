package com.chenxing.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Date:2020/10/24
 * Author:Ling
 * Description:表示购物车的类
 */
public class Cart implements Serializable { //取消session钝化

    //购物车中所有的购物项，以购物项中图书的id为键，以购物项为值
    private Map<String,CartItem> map=new HashMap<>();

    //当前购物车所有图书的总数量
    private Integer totalCount;

    //购物车中的所有图书的总金额
    private Double totalAmount;

    public Map<String, CartItem> getMap() {
        return map;
    }

    public void setMap(Map<String, CartItem> map) {
        this.map = map;
    }

    public Integer getTotalCount() {

        totalCount=0;

        for (CartItem cartItem : getCartItemList()) {
            totalCount+=cartItem.getCount();
        }
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Double getTotalAmount() {
        BigDecimal bigDecimal1=new BigDecimal("0.0"); //处理精度问题，防止出现长精度
        for (CartItem cartItem : getCartItemList()) {
            BigDecimal bigDecimal2=new BigDecimal(cartItem.getAmount()+"");
            bigDecimal1=bigDecimal1.add(bigDecimal2);
        }
        return bigDecimal1.doubleValue();
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    //获取所有的购物项
    public List<CartItem> getCartItemList()
    {
        return new ArrayList<>(map.values()); //map集合的键转化为list
    }

    //加入购物车
    public void addCartItem(Book book)
    {
        //根据图书id获取map集合中图书所对应的购物项
        CartItem cartItem = map.get(book.getId() + "");
        //判断购物车中是否存在此购物项
        if (cartItem==null)
        {
            //没有此购物项，是新添加的购物项（需要将book对象转化为购物项）
            cartItem=new CartItem();
            cartItem.setBook(book);
            cartItem.setCount(1);
            //将购物项添加到购物车的map中
            map.put(book.getId()+"",cartItem);
        }
        else
        {
            //购物车中已有此购物项，将此购物项的数量+1
            cartItem.setCount(cartItem.getCount()+1);
        }
    }

    //删除购物项
    public void deleteCartItem(String bookId)
    {
        map.remove(bookId);
    }

    //清空购物车
    public void clearCart()
    {
        map.clear();
    }

    //更新购物项的数量
    public void updateCount(String bookId,String countStr)
    {
        try {
            Integer count = Integer.parseInt(countStr);
            if(count>0){
                map.get(bookId).setCount(count);
            }
            else if(count==0){
                map.remove(bookId);
            }
        } catch (NumberFormatException e) {        }
    }

    @Override
    public String toString() {
        return "Cart{" +
                "map=" + map +
                ", totalCount=" + totalCount +
                ", totalAmount=" + totalAmount +
                '}';
    }
}
