package com.chenxing.bean;

/**
 * Date:2020/10/28
 * Author:Ling
 * Description:
 */
public class Order {
    private String id;
    private String createTime;
    private Integer totalCount;
    private Double totalAmount;
    private Integer status;
    private Integer userId;

    public Order() {
    }

    public Order(String id, String createTime, Integer totalCount, Double totalAmount, Integer status, Integer userId) {
        this.id = id;
        this.createTime = createTime;
        this.totalCount = totalCount;
        this.totalAmount = totalAmount;
        this.status = status;
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getcreateTime() {
        return createTime;
    }

    public void setcreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id='" + id + '\'' +
                ", createTime='" + createTime + '\'' +
                ", totalCount=" + totalCount +
                ", totalAmount=" + totalAmount +
                ", status=" + status +
                ", userId=" + userId +
                '}';
    }
}
