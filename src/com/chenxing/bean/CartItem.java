package com.chenxing.bean;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Date:2020/10/24
 * Author:Ling
 * Description:表示购物项的类
 */
public class CartItem implements Serializable { //取消session钝化

    //购物项所对应的图书信息
    private Book book;
    //购物项中图书所购买的数量
    private Integer count;
    //购物项中购买图书所花费的金额
    private Double amount;

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Double getAmount() {
        BigDecimal bigDecimal1=new BigDecimal(book.getPrice()+""); //处理精度问题，防止出现长精度
        BigDecimal bigDecimal2=new BigDecimal(count+"");
        BigDecimal multiply = bigDecimal1.multiply(bigDecimal2);
        return multiply.doubleValue();
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "CartItem{" +
                "book=" + book +
                ", count=" + count +
                ", amount=" + amount +
                '}';
    }
}
