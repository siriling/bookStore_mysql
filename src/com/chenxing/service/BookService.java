package com.chenxing.service;

import com.chenxing.bean.Book;
import com.chenxing.bean.Page;

import java.util.List;

/**
 * Date:2020/10/11
 * Author:Ling
 * Description:
 */
public interface BookService {
    //完善Page对象,按照价格区间查询
    Page<Book> getPageByPrice(Page<Book> page,Integer minPrice,Integer maxPrice);
    //根据Page对象获取完整的Page对象（完善Page对象）
    Page<Book> getPage(Page<Book> page);

//    //根据pageNo和pageSize获取图书的分页信息
//    List<Book> getBookPage(Integer pageNo,Integer pageSize);

    //获取图书信息的总记录数
    Integer getBookCount();
    //查询所有的图书信息
    List<Book> getBookList();
    //添加图书信息
    void addBook(Book book);
    //删除图书信息
    void deleteBook(String id);
    //根据图书id获取要修改的图书信息
    Book getBookByBookId(String id);
    //修改图书信息
    void updateBook(Book book);
}
