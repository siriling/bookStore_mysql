package com.chenxing.service;

import com.chenxing.bean.User;

/**
 * Date:2020/10/4
 * Author:Ling
 * Description:service接口
 */
public interface UserService {
    //验证登录
    User login(String username,String password);

    //实现注册功能
    boolean registUser(User user);

    //订单内容需返回用户名字
    User  getUsername(Integer id);
}
