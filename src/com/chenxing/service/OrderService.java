package com.chenxing.service;

import com.chenxing.bean.Cart;
import com.chenxing.bean.Order;
import com.chenxing.bean.OrderItem;
import com.chenxing.bean.User;

import java.util.List;

/**
 * Date:2020/10/28
 * Author:Ling
 * Description:
 */
public interface OrderService {
    //结账，并将订单编号返回
    String checkout(Cart cart, User user);

    //查看我的订单
    List<Order> getMyOrder(Integer userId);

    //查看订单详情
    List<OrderItem> getOrderDetails(String orderId);

    //收货，将订单状态由1-->2
    void takeOrder(String orderId);

    //查看所有订单
    List<Order> getAllOrder();

    //发货，将订单状态由0-->1
    void sendOrder(String orderId);
}
