package com.chenxing.service.impl;

import com.chenxing.bean.Book;
import com.chenxing.bean.Page;
import com.chenxing.dao.BookDao;
import com.chenxing.dao.impl.BookDaoImpl;
import com.chenxing.service.BookService;

import java.util.List;

/**
 * Date:2020/10/11
 * Author:Ling
 * Description:
 */
public class BooksServiceImpl implements BookService {

    private BookDao bookDao=new BookDaoImpl();

//    @Override
//    public List<Book> getBookPage(Integer pageNo, Integer pageSize) {
//        return bookDao.getBookPage(pageNo,pageSize);
//    }

    @Override
    public Page<Book> getPageByPrice(Page<Book> page, Integer minPrice, Integer maxPrice) {
        //获取价格区间的总记录数
        Integer totalCount = bookDao.getCountByPrice(minPrice, maxPrice);
        //将totalCount封装到page对象中
        page.setTotalCount(totalCount);
        //获取价格区间条件下的图书信息
        List<Book> list = bookDao.getPageByPrice(page, minPrice, maxPrice);
        //将list集合放在page对象中
        page.setList(list);
        return page;
    }

    @Override
    public Page<Book> getPage(Page<Book> page) {
        //获取总记录数
        Integer bookCount = bookDao.getBookCount();
        /**
         * 1.将总记录数赋值给Page对象中的totalCount
         * 2.通过totalCount和pageSize获取totalPage
         * 3.通过totalPage和pageSize获取pageNo
         * 4.通过pageNo和pageSize获取List
         */
        page.setTotalCount(bookCount); //将总记录数赋值给Page对象中的totalCount
        List<Book> list = bookDao.getPage(page);//通过pageNo和pageSize获取List
        //将list赋值给page对象中的list
        page.setList(list);
        return page;
    }

    @Override
    public Integer getBookCount() {
        return bookDao.getBookCount();
    }

    @Override
    public List<Book> getBookList() {
        return bookDao.getBookList();
    }

    @Override
    public void addBook(Book book) {
        bookDao.addBook(book);
    }

    @Override
    public void deleteBook(String id) {
        bookDao.deleteBook(id);
    }

    @Override
    public Book getBookByBookId(String id) {
        return bookDao.getBookByBookId(id);
    }

    @Override
    public void updateBook(Book book) {
        bookDao.updateBook(book);
    }
}
