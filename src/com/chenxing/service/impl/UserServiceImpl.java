package com.chenxing.service.impl;

import com.chenxing.bean.User;
import com.chenxing.dao.UserDao;
import com.chenxing.dao.impl.UserDaoImpl;
import com.chenxing.service.UserService;

/**
 * Date:2020/10/4
 * Author:Ling
 * Description:service实现类
 */
public class UserServiceImpl implements UserService {

    private UserDao userDao=new UserDaoImpl();

    @Override
    public User login(String username, String password)
    {
        return userDao.login(username, password);
    }

    @Override
    public boolean registUser(User user)
    {
        //根据用户名查询用户信息
        User userBySQL = userDao.checkUsername(user.getUsername());
        //判断用户名是否可用
        if(userBySQL==null) //用户名可用
        {
            userDao.registUser(user);
            return true;
        }
        else //用户名不可用
        return false;
    }

    @Override
    public User getUsername(Integer id) {
        return userDao.getUsername(id);
    }
}
