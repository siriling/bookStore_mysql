package com.chenxing.utils;

import jakarta.servlet.http.HttpServletRequest;

/**
 * Date:2020/10/17
 * Author:Ling
 * Description:
 */
public class WebUtils {
    public static String getPath(HttpServletRequest request)
    {
        //获取请求路径
        String uri = request.getRequestURI();
        //获取请求路径中所拼接的请求参数
        String queryString = request.getQueryString();
        //判断请求参数是否有&pageNo
        if(queryString.contains("&pageNo"))
        {
            //将请求参数中的&pageNo截掉
            queryString=queryString.substring(0,queryString.indexOf("&pageNo"));
        }
        //将动态获取的请求路径返回
        return uri+"?"+queryString;
    }
}
