package com.chenxing.filter;

import com.chenxing.utils.JDBCUtils;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Date:2020/10/31
 * Author:Ling
 * Description:事物过滤
 */
public class TransactionFilter extends HttpFilter {

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        Connection conn=null;
        try {
            //获取链接对象
            conn = JDBCUtils.getConn();
            //将自动提交的事务进行关闭
            conn.setAutoCommit(false);
            //放行，执行servlet中的service()，实现操作的整个工程
            chain.doFilter(request,response);
            //执行到此位置，说明实现功能过程中没有问题，需要提交事物
            conn.commit();
        } catch (Exception e) {
            e.printStackTrace();
            //执行到此位置，说明实现功能的过程中有异常，需要回滚事物
            try {
                conn.rollback();
                System.out.println("执行失败，回滚");
                response.sendRedirect(request.getContextPath()+"/pages/error/error.jsp");
            } catch (SQLException e1) {
                e1.printStackTrace();
            }finally {
                //将唯一一个Connection对象关掉，删掉BaseDao中所有的finally
                JDBCUtils.closeConn(conn); //添加了事物后
            }
        }
    }
}
