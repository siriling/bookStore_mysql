package com.chenxing.filter;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

/**
 * Date:2020/10/28
 * Author:Ling
 * Description:
 */
public class LoginFilter extends HttpFilter {

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpSession session = request.getSession();
        Object user = session.getAttribute("user");
        if(user==null)
        {
            //未登录
            request.setAttribute("errorMsg","请先登录");
            //转发到登录页面
            request.getRequestDispatcher("/pages/user/login.jsp").forward(request,response);
        }
        else
        {
            chain.doFilter(request,response);
        }
    }
}
