package com.chenxing.filter;

import com.chenxing.bean.User;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

/**
 * Date:2020/10/28
 * Author:Ling
 * Description:管理员只需要进入后台管理界面，不需要访问客户端页面
 */
public class ManagerFilter2 extends HttpFilter {

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if(user==null)
        {
            chain.doFilter(request,response);
        }
        else if("root".equals(user.getUsername()))
        {
            //重定向到管理员页面
            response.sendRedirect(request.getContextPath()+"/pages/manager/manager.jsp");
        }
        else
        {
            chain.doFilter(request,response);
        }
    }
}
