package com.chenxing.filter;

import com.chenxing.bean.User;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

/**
 * Date:2020/10/28
 * Author:Ling
 * Description:拦截不是管理员的用户
 */
public class ManagerFilter1 extends HttpFilter {

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if(user==null)
        {
            //未登录
            request.setAttribute("errorMsg","请先登录");
            //转发到登录页面
            request.getRequestDispatcher("/pages/user/login.jsp").forward(request,response);
        }
        else if(!"root".equals(user.getUsername()))
        {
            //不是管理员，拦截
            request.setAttribute("errorMsg","您不是管理员");
            //重定向到首页
            response.sendRedirect(request.getContextPath());
        }
        else
        {
            chain.doFilter(request,response);
        }
    }
}
