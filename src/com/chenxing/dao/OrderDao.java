package com.chenxing.dao;

import com.chenxing.bean.Order;

import java.util.List;

/**
 * Date:2020/10/28
 * Author:Ling
 * Description:
 */
public interface OrderDao {
    //保存订单
    void saveOrder(Order order);

    //获取我的订单
    List<Order> getMyOrder(Integer userId);

    //获取所有订单
    List<Order> getAllOrder();

    //更新订单的状态
    void updateStatus(String orderId,Integer status);
}
