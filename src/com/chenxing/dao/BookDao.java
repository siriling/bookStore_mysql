package com.chenxing.dao;

import com.chenxing.bean.Book;
import com.chenxing.bean.Page;

import java.util.List;

/**
 * Date:2020/10/11
 * Author:Ling
 * Description:图书接口
 */
public interface BookDao {
    //通过价格区间查询图书的分页信息
    List<Book> getPageByPrice(Page<Book> page,Integer minPrice,Integer maxPrice);

    //通过价格区间查询图书信息的总记录数
    Integer getCountByPrice(Integer minPrice,Integer maxPrice);

    //根据Page对象获取分页信息
    List<Book> getPage(Page<Book> page);

//    //分页（根据当前页的页码pageNo和每页显示的条数pageSize获取图书的分页信息）
//    List<Book> getBookPage(Integer pageNo,Integer pageSize);

    //获取图书信息的总记录数
    Integer getBookCount();
    //查询所有的图书信息
    List<Book> getBookList();
    //添加图书信息
    void addBook(Book book);
    //删除图书信息
    void deleteBook(String id);
    //根据图书id获取要修改的图书信息
    Book getBookByBookId(String id);
    //修改图书信息
    void updateBook(Book book);
    //更新库存和销量
    void  updateSalesAndStock(Integer bookId,Integer count);
}
