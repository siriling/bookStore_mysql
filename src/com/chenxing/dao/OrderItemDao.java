package com.chenxing.dao;

import com.chenxing.bean.OrderItem;
import java.util.List;

/**
 * Date:2020/10/28
 * Author:Ling
 * Description:
 */
public interface OrderItemDao {
    //保存订单项
    void saveOrderItem(OrderItem orderItem);

    //查看订单详情
    List<OrderItem> getOrderDetails(String orderId);

}
