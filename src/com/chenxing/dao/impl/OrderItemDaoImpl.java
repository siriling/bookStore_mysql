package com.chenxing.dao.impl;

import com.chenxing.bean.OrderItem;
import com.chenxing.dao.BaseDao;
import com.chenxing.dao.OrderItemDao;

import java.util.List;

/**
 * Date:2020/10/28
 * Author:Ling
 * Description:
 */
public class OrderItemDaoImpl extends BaseDao implements OrderItemDao {
    @Override
    public void saveOrderItem(OrderItem orderItem) {
        String sql="insert into bs_orderItem values(null,?,?,?,?,?,?,?,?)";
        update(sql,orderItem.getcreateTime(),orderItem.getTitle(),orderItem.getAuthor(),orderItem.getPrice(),orderItem.getImgPath(),orderItem.getCount(),orderItem.getAmount(),orderItem.getOrderId());
    }

    @Override
    public List<OrderItem> getOrderDetails(String orderId) {
        String sql="select * from bs_orderItem where orderId=?";
        return getBeanList(OrderItem.class,sql,orderId);
    }
}
