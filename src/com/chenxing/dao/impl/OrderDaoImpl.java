package com.chenxing.dao.impl;

import com.chenxing.bean.Order;
import com.chenxing.dao.BaseDao;
import com.chenxing.dao.OrderDao;

import java.util.List;

/**
 * Date:2020/10/28
 * Author:Ling
 * Description:
 */
public class OrderDaoImpl extends BaseDao implements OrderDao {
    @Override
    public void saveOrder(Order order) {
        String sql="insert into bs_order values(?,?,?,?,?,?)";
        update(sql,order.getId(),order.getcreateTime(),order.getTotalAmount(),order.getTotalAmount(),order.getStatus(),order.getUserId());
    }

    @Override
    public List<Order> getMyOrder(Integer userId) {
        String sql="select * from bs_order where userId=?";
        return getBeanList(Order.class,sql,userId);
    }

    @Override
    public List<Order> getAllOrder() {
        String sql="select * from bs_order";
        return getBeanList(Order.class,sql);
    }

    @Override
    public void updateStatus(String orderId, Integer status) {
        String sql="update bs_order set status=? where id=?";
        update(sql,status,orderId);
    }
}
