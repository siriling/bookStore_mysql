package com.chenxing.dao;

import com.chenxing.bean.User;

/**
 * Date:2020/10/4
 * Author:Ling
 * Description:dao接口
 */
public interface UserDao {
    //通过用户名和密码查询用户信息
    User login(String username,String password);

    //通过用户名查询用户信息（检查用户名是否已经被注册）
    User checkUsername(String username);

    //实现注册功能
    void registUser(User user);

    //订单内容需返回用户名字
    User  getUsername(Integer id);
}
