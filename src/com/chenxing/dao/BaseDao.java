package com.chenxing.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import com.chenxing.utils.JDBCUtils;
import org.apache.commons.dbutils.handlers.ScalarHandler;

/**
 * 封装对数据库数据操作的基本方法
 * 
 *
 */
public class BaseDao {
	//创建QueryRunner对象
	private QueryRunner runner = new QueryRunner();
	
	/**
	 * 增删改方法
	 * 	- 凡是不能确定的数据都是用参数传递
	 */
	public int update(String sql , Object...params) {
		Connection conn = JDBCUtils.getConn();
		int i = 0;
		try {
			//此方法为实现增删改的方法，params是按照顺序为通配符所赋的值
			i = runner.update(conn, sql, params);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		// 如果i>0代表增删改成功，<=0代表失败
		return i;
	}
	/**
	 * 查询一条记录 封装为一个对象
	 */
	public <T>T getBean(Class<T> type, String sql , Object...params) {
		Connection conn = JDBCUtils.getConn();
		T t = null;
		// 参数1：数据库连接 ， 参数2：sql语句 ， 参数4：sql语句占位符参数列表 ， 参数3：查询结果解析工具类对象[type就是讲查询到记录封装对象的类型  ]
		try {
			t = runner.query(conn, sql, new BeanHandler<>(type), params);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		//如果t==null代表查询失败 ， 如果不为null代表查询成功
		return t;
	}
	/**
	 * 查询多条记录封装为对象的集合
	 */
	public <T>List<T> getBeanList(Class<T> type , String sql , Object...params) {
		Connection conn = JDBCUtils.getConn();
		List<T> list = null;
		try {
			list = runner.query(conn, sql, new BeanListHandler<>(type), params);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return list;
	}
	/**
	 * 查询单个数据（单行单列）
	 */
	public Object getSingleData(String sql,Object... params) {
		Connection conn = JDBCUtils.getConn();
		Object object=null;
		try {
			object=runner.query(conn,sql,new ScalarHandler<>(),params);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return object;
	}
}
