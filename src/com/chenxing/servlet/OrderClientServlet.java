package com.chenxing.servlet;

import com.chenxing.bean.Cart;
import com.chenxing.bean.Order;
import com.chenxing.bean.OrderItem;
import com.chenxing.bean.User;
import com.chenxing.service.OrderService;
import com.chenxing.service.impl.OrderServiceImpl;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Date:2020/10/28
 * Author:Ling
 * Description:
 */
public class OrderClientServlet extends BaseServlet {

    private OrderService orderService=new OrderServiceImpl();

    //结账
    protected void checkout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取session
        HttpSession session = request.getSession();
        //获取session中的购物车对象cart和用户对象user
        Cart cart = (Cart) session.getAttribute("cart");
        User user = (User) session.getAttribute("user");
        //调用service处理
        String orderId = orderService.checkout(cart, user);
        //将购物车信息从session中删除
        session.removeAttribute("cart");
        //将订单编号存储在session中
        session.setAttribute("orderId",orderId);
        //重定向跳转到结账成功页面
        response.sendRedirect(request.getContextPath()+"/pages/cart/checkout.jsp");

    }

    //查看我的订单
    protected void getMyOrder(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //获取session
        HttpSession session = request.getSession();
        //获取当前登录的用户信息
        User user = (User) session.getAttribute("user");
        //调用service处理业务逻辑
        List<Order> list = orderService.getMyOrder(user.getId());
        //将我的订单放在请求域中
        request.setAttribute("list",list);
        //转发跳转到order_client.jsp
        request.getRequestDispatcher("/pages/client/order_client.jsp").forward(request,response);

    }

    //查看订单详情
    protected void getOrderDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //获取请求参数
        String orderId = request.getParameter("orderId");
        //调用service处理
        List<OrderItem> list = orderService.getOrderDetails(orderId);
        //将订单详情放在请求域中
        request.setAttribute("list",list);
        //转发跳转到订单详情页面
        request.getRequestDispatcher("/pages/client/order_details.jsp").forward(request,response);
    }

    //收货
    protected void takeOrder(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //获取请求参数
        String orderId = request.getParameter("orderId");
        //调用service处理
        orderService.takeOrder(orderId);
        //重定向到来源页面
        response.sendRedirect(request.getHeader("referer"));
    }

}
