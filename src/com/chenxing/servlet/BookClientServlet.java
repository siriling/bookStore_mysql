package com.chenxing.servlet;

import com.chenxing.bean.Book;
import com.chenxing.bean.Page;
import com.chenxing.service.BookService;
import com.chenxing.service.impl.BooksServiceImpl;
import com.chenxing.utils.WebUtils;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Date:2020/10/16
 * Author:Ling
 * Description:
 */
public class BookClientServlet extends BaseServlet {

    private BookService bookService=new BooksServiceImpl();

    protected void getPageByPrice(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取请求参数
        String pageNoStr = request.getParameter("pageNo");
        String min = request.getParameter("min");
        String max = request.getParameter("max");

        //将价格区间转换为数值
        Integer minPrice=0;
        Integer maxPrice=Integer.MAX_VALUE;
        try {
            minPrice=Integer.parseInt(min);
        } catch (NumberFormatException e) {
//            e.printStackTrace();
        }
        try {
            maxPrice=Integer.parseInt(max);
        } catch (NumberFormatException e) {
//            e.printStackTrace();
        }

        //创建Page对象
        Page<Book> pageInfo=new Page<>();
        pageInfo.setPageNoStr(pageNoStr);
        //动态获取当前的请求路径
        String path = WebUtils.getPath(request);
        pageInfo.setPath(path);
        //调用service处理业务逻辑
        Page<Book> page = bookService.getPageByPrice(pageInfo,minPrice,maxPrice);
        //将封装了所有信息的Page对象放在请求域中
        request.setAttribute("page",page);
        //转发跳转到book_manager.jsp
        request.getRequestDispatcher("/pages/client/book_client.jsp").forward(request,response);
    }

    protected void getPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取请求参数
        String pageNoStr = request.getParameter("pageNo");
        //创建Page对象
        Page<Book> pageInfo=new Page<>();
        pageInfo.setPageNoStr(pageNoStr);
        //动态获取当前的请求路径
        String path = WebUtils.getPath(request);
        pageInfo.setPath(path);
        //调用service处理业务逻辑
        Page<Book> page = bookService.getPage(pageInfo);
        //将封装了所有信息的Page对象放在请求域中
        request.setAttribute("page",page);
        //转发跳转到book_manager.jsp
        request.getRequestDispatcher("/pages/client/book_client.jsp").forward(request,response);
    }
}
