package com.chenxing.servlet;

import com.chenxing.bean.Order;
import com.chenxing.bean.OrderItem;
import com.chenxing.bean.User;
import com.chenxing.service.OrderService;
import com.chenxing.service.UserService;
import com.chenxing.service.impl.OrderServiceImpl;
import com.chenxing.service.impl.UserServiceImpl;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Date:2020/10/28
 * Author:Ling
 * Description:
 */
public class OrderManagerServlet extends BaseServlet {

    private OrderService orderService=new OrderServiceImpl();
    private UserService userService=new UserServiceImpl();

    //订单管理（查看所有订单）
    protected void getAllOrder(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //调用service处理
        List<Order> list = orderService.getAllOrder();

        //查询出用户名一起显示
        List<User> users=new ArrayList<>();
        for (Order order : list) {
            User user = userService.getUsername(order.getUserId());
            users.add(user);
        }

        //将所有的订单信息放在请求域中
        request.setAttribute("list",list);
        //将所有的用户名信息放在请求域中
        request.setAttribute("users",users);
        //转发到order_manager.jsp
        request.getRequestDispatcher("/pages/manager/order_manager.jsp").forward(request,response);
    }

    //发货
    protected void sendOrder(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //获取请求参数
        String orderId = request.getParameter("orderId");
        //调用service处理
        orderService.sendOrder(orderId);
        //重定向到来源页面
        response.sendRedirect(request.getHeader("referer"));
    }

    //查看详情
    protected void getOrderDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //获取请求参数
        String orderId = request.getParameter("orderId");
        //调用service处理
        List<OrderItem> list = orderService.getOrderDetails(orderId);
        //将订单详情放在请求域中
        request.setAttribute("list",list);
        //转发跳转到订单详情页面
        request.getRequestDispatcher("/pages/manager/order_details.jsp").forward(request,response);

    }
}
