package com.chenxing.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;

/**
 * Date:2020/10/7
 * Author:Ling
 * Description:根据每次请求所传输的请求参数，自动执行相应的方法
 */
public class BaseServlet extends HttpServlet {
    @Override
    final protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //设置编码
        request.setCharacterEncoding("UTF-8");
        //获取请求参数
        String methodName = request.getParameter("method");

        try {
            //通过methodName获取方法，此方法的方法名跟methodName一致
            Method method = this.getClass().getDeclaredMethod(methodName, HttpServletRequest.class, HttpServletResponse.class);
            //忽略该方法的访问权限
            method.setAccessible(true);
            //执行此方法
            method.invoke(this,request,response);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    final protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
