package com.chenxing.servlet;

import com.chenxing.bean.User;
import com.chenxing.service.UserService;
import com.chenxing.service.impl.UserServiceImpl;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Date:2020/10/7
 * Author:Ling
 * Description:
 */
public class UserServlet extends BaseServlet {

    private UserService userService=new UserServiceImpl();

    //登录
    protected void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //获取请求参数
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        //调用service来处理业务逻辑
        User user = userService.login(username, password);
        //判断user是否为空
        if(user==null)
        {
            //在请求域中共享提示信息
            request.setAttribute("errorMsg","用户名或密码错误");

            //登录失败，需要转发到登录界面
            request.getRequestDispatcher("/pages/user/login.jsp").forward(request,response);
        }
        else
        {
            //将用户信息放在session中进行共享
            HttpSession session = request.getSession();
            session.setAttribute("user",user);
            //登录成功，需要重定向到登录成功页面（管理员到管理界面，用户到成功页面）
            if (user.getUsername().equals("root")) {
                response.sendRedirect(request.getContextPath()+"/pages/manager/manager.jsp");
            }
            else {
                response.sendRedirect(request.getContextPath()+"/pages/user/login_success.jsp");
            }
        }
    }
    //注册
    protected void regist(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //获取用户输入的验证码
        String code = request.getParameter("code");
        //获取session
        HttpSession session = request.getSession();
        //获取正确的验证码
        String sessionCode = (String) session.getAttribute("sessionCode");
        //比较验证码是否正确
        if(code.equals(sessionCode))
        {
            //正确实现注册功能
            //获取请求参数
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String email = request.getParameter("email");

            //将用户信息封装到User中
            User user=new User(null,username,password,email);
            //调用service来处理业务逻辑
            boolean flag = userService.registUser(user);
            //判断注册是否成功
            if(flag)
            {
                //注册成功,需要重定向到注册成功页面
                response.sendRedirect(request.getContextPath()+"/pages/user/regist_success.jsp");
            }
            else
            {
                //将提示信息放在请求域中
                request.setAttribute("errorMsg","用户名已经被注册");
                //注册失败,需要转发到注册页面
                request.getRequestDispatcher("/pages/user/regist.jsp").forward(request,response);
            }
        }
        else
        {
            //错误，转发到注册页面并显示验证码的错误提示
            request.setAttribute("errorMsg","验证码错误");
            request.getRequestDispatcher("/pages/user/regist.jsp").forward(request,response);

        }

    }

    //注销（将session中和用户相关信息删除）
    protected void logout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession();
//        session.removeAttribute("user"); //将用户信息从session中删除
        //使session失效
        session.invalidate();
        response.sendRedirect(request.getContextPath()+"/index.jsp");
    }

    //注册（Ajax实现）
    protected void regist_ajax(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //获取用户输入的验证码
        String code = request.getParameter("code");
        //获取session
        HttpSession session = request.getSession();
        //获取正确的验证码
        String sessionCode = (String) session.getAttribute("sessionCode");
        //比较验证码是否正确
        if(code.equals(sessionCode))
        {
            //正确实现注册功能
            //获取请求参数
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String email = request.getParameter("email");

            //将用户信息封装到User中
            User user=new User(null,username,password,email);
            //调用service来处理业务逻辑
            boolean flag = userService.registUser(user);
            //判断注册是否成功
            if(flag)
            {
                //验证码错误，响应到客户端一个标识success
                response.getWriter().print("success");
            }
            else
            {
                //注册失败,响应到客户端一个标识username
                response.getWriter().print("username");
            }
        }
        else
        {
            //验证码错误，响应到客户端一个标识code
            response.getWriter().print("code");
        }

    }
}
