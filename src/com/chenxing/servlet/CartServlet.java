package com.chenxing.servlet;

import com.chenxing.bean.Book;
import com.chenxing.bean.Cart;
import com.chenxing.service.BookService;
import com.chenxing.service.impl.BooksServiceImpl;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Date:2020/10/24
 * Author:Ling
 * Description:
 */
public class CartServlet extends BaseServlet {

    private BookService bookService = new BooksServiceImpl();

    //添加图书到购物车
    protected void addCartItem(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取请求参数
        String bookId = request.getParameter("bookId");
        //获取购物车对象，必须从session中获取，以保证在一次会话中操作的购物车是同一个
        HttpSession session = request.getSession();
        Cart cart = (Cart) session.getAttribute("cart");
        //判断购物车是否为第一次添加图书信息
        if (cart == null) {
            //第一次添加图书信息，重新创建购物车对象
            cart = new Cart();
        }

        //根据bookId获取图书信息
        Book book = bookService.getBookByBookId(bookId);
        //将图书信息添加到购物车中
        cart.addCartItem(book);

        //将购物车对象放在session中共享
        session.setAttribute("cart", cart);
        //将刚刚添加到购物车中的图书标题共享在session中，方便在页面中显示
        session.setAttribute("bookTitle", book.getTitle());
        //重定向到此操作的来源页面
        response.sendRedirect(request.getHeader("referer"));
    }

    //删除购物项
    protected void deleteCartItem(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //获取请求参数
        String bookId = request.getParameter("bookId");
        //获取session对象
        HttpSession session = request.getSession();
        //从session中获取购物车对象
        Cart cart = (Cart) session.getAttribute("cart");
        //根据bookId从购物车中删除购物项
        cart.deleteCartItem(bookId);
        //重定向到此操作的来源页面
        response.sendRedirect(request.getHeader("referer"));
    }

    //清空购物车
    protected void clearCart(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //获取session对象
        HttpSession session = request.getSession();
        //从session中获取购物车对象
        Cart cart = (Cart) session.getAttribute("cart");
        //清空购物车
        cart.clearCart();
        //重定向到此操作的来源页面
        response.sendRedirect(request.getHeader("referer"));
    }

    //更新购物项的数量
    protected void updateCount(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        //获取请求参数
        String bookId = request.getParameter("bookId");
        String countStr = request.getParameter("count");

        //获取session对象
        HttpSession session = request.getSession();
        //从session中获取购物车对象
        Cart cart = (Cart) session.getAttribute("cart");

        //更新购物项的数量
        cart.updateCount(bookId,countStr);
        //重定向到此操作的来源页面
        response.sendRedirect(request.getHeader("referer"));
    }
}
