package com.chenxing.servlet;

import com.chenxing.bean.Book;
import com.chenxing.bean.Page;
import com.chenxing.service.BookService;
import com.chenxing.service.impl.BooksServiceImpl;
import com.chenxing.utils.WebUtils;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Date:2020/10/11
 * Author:Ling
 * Description:
 */
public class BookManagerServlet extends BaseServlet {

    private BookService bookService = new BooksServiceImpl();

    //通过Page处理获取图书的分页信息请求
    protected void getPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取请求参数
        String pageNoStr = request.getParameter("pageNo");
        //创建Page对象
        Page<Book> pageInfo=new Page<>();
        pageInfo.setPageNoStr(pageNoStr);
        //动态获取当前的请求路径mm\ 
        String path = WebUtils.getPath(request);
        pageInfo.setPath(path);
        //调用service处理业务逻辑
        Page<Book> page = bookService.getPage(pageInfo);
        //将封装了所有信息的Page对象放在请求域中
        request.setAttribute("page",page);
        //转发跳转到book_manager.jsp
        request.getRequestDispatcher("/pages/manager/book_manager.jsp").forward(request,response);

    }
//    //处理获取图书的分页信息请求
//    protected void getBookPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        //获取请求参数
//        String pageNoStr = request.getParameter("pageNo");
//        //设置每页显示的条数
//        Integer pageSize=4;
//        //调用service获取图书的总记录数
//        Integer bookCount = bookService.getBookCount();
//        //通过总记录数count和每页显示的条数获取总页数
//        Integer totalPage=0;
//        if(bookCount%pageSize==0)
//        {
//            totalPage=bookCount/pageSize;
//        }
//        else
//        {
//            totalPage=bookCount/pageSize+1;
//        }
//        //获取pageNo和pageSize
//        Integer pageNo=1;
//        try {
//            //能转换就转换转换不了也没事，pageNo有默认值
//            pageNo=Integer.parseInt(pageNoStr);
//        } catch (NumberFormatException e) {}
//        //若pageN小于等于0，默认访问第一页
//        if(pageNo<=0)
//        {
//            pageNo=1;
//        }
//        //若pageNo大于总页数的，默认访问最后一页
//        if(pageNo>=totalPage)
//        {
//            pageNo=totalPage;
//        }
//
//        //调用service处理业务逻辑
//        List<Book> bookList = bookService.getBookPage(pageNo, pageSize);
//        //将当前页的页码pageNo,总页数totalPage，总记录数bookCount放在请求域中，在页面中进过判断计算出与分页相关的超链接信息
//        request.setAttribute("pageNo",pageNo);
//        request.setAttribute("totalPage",totalPage);
//        request.setAttribute("bookCount",bookCount);
//        //将数据存储在请求域中
//        request.setAttribute("bookList",bookList);
//        //转发跳转到book_manager.jsp
//        request.getRequestDispatcher("/pages/manager/book_manager.jsp").forward(request,response);
//
//    }
        //处理获取所有图书信息的请求
    protected void getBookList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //调用service处理业务逻辑
        List<Book> bookList = bookService.getBookList();
        //将图书信息在请求域request中进行共享
        request.setAttribute("bookList", bookList);
        //使用转发跳转到图书列表页面book_manager.jsp
        request.getRequestDispatcher("/pages/manager/book_manager.jsp").forward(request, response);
    }

    //处理添加图书信息的请求
    protected void addBook(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取请求参数
        String title = request.getParameter("title");
        String author = request.getParameter("author");
        String price = request.getParameter("price");
        String sales = request.getParameter("sales");
        String stock = request.getParameter("stock");
        String imgPath=request.getParameter("imgPath");

        //将图书信息封装到Book对象中
        Book book = new Book(null, title, author, Double.parseDouble(price), Integer.parseInt(sales), Integer.parseInt(stock), imgPath);
        //调用service处理
        bookService.addBook(book);
        //使用重定向到列表功能(而不是列表页面，是从servlet-->book_manager.jsp的过程)
        response.sendRedirect(request.getContextPath() + "/BookManagerServlet?method=getBookList");
    }
    //修改图书
    protected void updateBook(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取请求参数
        String id = request.getParameter("id");
        String title = request.getParameter("title");
        String author = request.getParameter("author");
        String price = request.getParameter("price");
        String sales = request.getParameter("sales");
        String stock = request.getParameter("stock");
        String imgPath=request.getParameter("imgPath");

        //将图书信息封装到Book对象中
        Book book = new Book(Integer.parseInt(id), title, author, Double.parseDouble(price), Integer.parseInt(sales), Integer.parseInt(stock), imgPath);
        //调用service处理
        bookService.updateBook(book);
        //使用重定向到列表功能(而不是列表页面，是从servlet-->book_manager.jsp的过程)
        response.sendRedirect(request.getContextPath() + "/BookManagerServlet?method=getBookList");
    }

    //处理删除图书信息请求
    protected void deleteBook(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取请求参数
        String id = request.getParameter("id");
        //调用service处理业务逻辑
        bookService.deleteBook(id);
        //重定向到列表功能
        response.sendRedirect(request.getContextPath() + "/BookManagerServlet?method=getPage&pageNo=1");
    }

    //处理修改图书信息请求（根据图书id获取图书信息，并在修改页面进行回显）
    protected void toUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取请求参数
        String id = request.getParameter("id");
        //调用service处理业务逻辑
        Book book = bookService.getBookByBookId(id);
        //将要修改的图书信息共享到请求域中
        request.setAttribute("book",book);
        //转发到修改页面
        request.getRequestDispatcher("/pages/manager/book_edit.jsp").forward(request, response);
    }

    //处理添加和修改功能（将添加和修改功能合并）
    protected void editBook(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取请求参数
        String id = request.getParameter("id");
        String title = request.getParameter("title");
        String author = request.getParameter("author");
        String price = request.getParameter("price");
        String sales = request.getParameter("sales");
        String stock = request.getParameter("stock");
        String imgPath=request.getParameter("imgPath");

        //判断id是否为null
        if(id==null) //添加
        {
            //将图书信息封装到Book对象中
            Book book = new Book(null, title, author, Double.parseDouble(price), Integer.parseInt(sales), Integer.parseInt(stock), imgPath);
            //调用service处理
            bookService.addBook(book);
        }
        else //修改
        {
            //将图书信息封装到Book对象中
            Book book = new Book(Integer.parseInt(id), title, author, Double.parseDouble(price), Integer.parseInt(sales), Integer.parseInt(stock), imgPath);
            //调用service处理
            bookService.updateBook(book);
        }
        //使用重定向到列表功能(而不是列表页面，是从servlet-->book_manager.jsp的过程)
        response.sendRedirect(request.getContextPath() + "/BookManagerServlet?method=getPage&pageNo=1");
    }
}
