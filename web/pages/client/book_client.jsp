<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<meta charset="UTF-8">
<title>书城首页</title>
<jsp:include page="/WEB-INF/include/base.jsp"></jsp:include>
	<script type="text/javascript">
		$(function ()
		{
			$("#searchBtn").click(function ()
			{
				var min=$("[name='min']").val();
				var max=$("[name='max']").val();
				location="BookClientServlet?method=getPageByPrice&min="+min+"&max="+max;
			});

			$(".addCartItemBtn").click(function ()
			{
				var bookId=$(this).val();
				location="CartServlet?method=addCartItem&bookId="+bookId;
			});
			//实现分页跳转页面功能
			$("#pageBtn").click(function ()
			{
				var pageNo=$("#pn_input").val();
				if (pageNo==null){
					pageNo=1;
				}
				location="BookClientServlet?method=getPage&pageNo="+pageNo;
			});
		});
	</script>
</head>
<body>
	<div id="header">
		<jsp:include page="/WEB-INF/include/logo.jsp"></jsp:include>
		<span class="wel_word">网上书城</span>
		<jsp:include page="/WEB-INF/include/client_head.jsp"></jsp:include>
	</div>
	
	<div id="main">
		<div id="book">
			<div class="book_cond">
				<form action="BookClientServlet" method="get">
					<input type="hidden" name="method" value="getPageByPrice">
					价格：<input type="text" name="min"> 元 - <input type="text" name="max"> 元 <button>查询</button>
				</form>
<%--				价格：<input type="text" name="min" value="${param.min}"> 元 - <input type="text" name="max" value="${param.max}"> 元 <button id="searchBtn">查询</button>--%>
			</div>
			<div style="text-align: center">
				<c:if test="${empty sessionScope.cart.map}">
					购物车中空空如也。。。
				</c:if>
				<c:if test="${not empty sessionScope.cart.map}">
					<span>您的购物车中有${sessionScope.cart.totalCount}件商品</span>
					<div>
						您刚刚将<span style="color: red">${sessionScope.bookTitle}</span>加入到了购物车中
					</div>
				</c:if>
			</div>
			<c:forEach items="${requestScope.page.list}" var="book">
				<div class="b_list">
					<div class="img_div">
						<img class="book_img" alt="" src="${pageScope.book.imgPath}" />
					</div>
					<div class="book_info">
						<div class="book_name">
							<span class="sp1">书名:</span>
							<span class="sp2">${pageScope.book.title}</span>
						</div>
						<div class="book_author">
							<span class="sp1">作者:</span>
							<span class="sp2">${pageScope.book.author}</span>
						</div>
						<div class="book_price">
							<span class="sp1">价格:</span>
							<span class="sp2">￥${pageScope.book.price}</span>
						</div>
						<div class="book_sales">
							<span class="sp1">销量:</span>
							<span class="sp2">${pageScope.book.sales}</span>
						</div>
						<div class="book_amount">
							<span class="sp1">库存:</span>
							<span class="sp2">${pageScope.book.stock}</span>
						</div>
						<div class="book_add">
							<button class="addCartItemBtn" value="${pageScope.book.id}">加入购物车</button>
							<input type="hidden" value="${pageScope.book.id}">
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
		<jsp:include page="/WEB-INF/include/page.jsp"></jsp:include>
	</div>

	<div id="bottom">
		<span>
			宸星书城.Copyright &copy;2020
		</span>
	</div>
</body>
</html>