<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<meta charset="UTF-8">
<title>我的订单</title>
<jsp:include page="/WEB-INF/include/base.jsp"></jsp:include>
<style type="text/css">
	h1 {
		text-align: center;
		margin-top: 200px;
	}
</style>
</head>
<body>
	<div id="header">
			<jsp:include page="/WEB-INF/include/logo.jsp"></jsp:include>
			<span class="wel_word">我的订单</span>
			<jsp:include page="/WEB-INF/include/client_head.jsp"></jsp:include>
	</div>
	
	<div id="main">
		<c:if test="${not empty requestScope.list}">
		<table>
			<tr>
				<td>日期</td>
				<td>金额</td>
				<td>详情</td>
				<td>状态</td>
			</tr>

			<c:forEach items="${requestScope.list}" var="order">
				<tr>
					<td style="width: 300px">${pageScope.order.createTime}</td>
					<td>${pageScope.order.totalAmount}</td>
					<td><a href="OrderClientServlet?method=getOrderDetails&orderId=${pageScope.order.id}">查看详情</a></td>
					<td>
						<c:if test="${pageScope.order.status==0}">等待发货</c:if>
						<c:if test="${pageScope.order.status==1}">
							<a href="OrderClientServlet?method=takeOrder&orderId=${pageScope.order.id}">点击收货</a>
						</c:if>
						<c:if test="${pageScope.order.status==2}">交易完成</c:if>
					</td>
				</tr>
			</c:forEach>
		</table>

		</c:if>

		<c:if test="${empty requestScope.list}">
			<br><br><br><br><br><br><br><br><br><br>
			<center>
				<h2>亲，没有订单哦</h2><a href="">去购物</a>
			</center>
		</c:if>

	</div>
	
	<div id="bottom">
		<span>
			宸星书城.Copyright &copy;2020
		</span>
	</div>
</body>
</html>