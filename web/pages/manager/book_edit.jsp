<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<meta charset="UTF-8">
<title>编辑图书</title>
<jsp:include page="/WEB-INF/include/base.jsp"></jsp:include>
<style type="text/css">
	h1 {
		text-align: center;
		margin-top: 200px;
	}
	
	h1 a {
		color: #ff0000;
	}
	
	input {
		text-align: center;
	}
</style>
</head>
<body>
		<div id="header">
			<jsp:include page="/WEB-INF/include/logo.jsp"></jsp:include>
			<span class="wel_word">编辑图书</span>
			<div><a href="javascript:history.go(-1);">&nbsp;取消</a></div>
			<jsp:include page="/WEB-INF/include/manager_head.jsp"></jsp:include>

		</div>
		
		<div id="main">
			<form action="BookManagerServlet?method=editBook" method="post">
				<c:if test="${not empty requestScope.book}">
					<input type="hidden" name="id" value="${requestScope.book.id}">
					<input type="hidden" name="imgPath" value="${requestScope.book.imgPath}"/>
				</c:if>
				<table>
					<tr>
						<td>名称</td>
						<td>价格</td>
						<td>作者</td>
						<td>销量</td>
						<td>库存</td>
						<td colspan="2">操作</td>
					</tr>		
					<tr>
						<td><input name="title" type="text" value="${requestScope.book.title}"/></td>
						<td><input name="price" type="text" value="${requestScope.book.price}"/></td>
						<td><input name="author" type="text" value="${requestScope.book.author}"/></td>
						<td><input name="sales" type="text" value="${requestScope.book.sales}"/></td>
						<td><input name="stock" type="text" value="${requestScope.book.stock}"/></td>
						<td><input type="submit" value="提交"/></td>
					</tr>
				</table>
			</form>
			
	
		</div>
		
		<div id="bottom">
			<span>
				宸星书城.Copyright &copy;2020
			</span>
		</div>
</body>
</html>