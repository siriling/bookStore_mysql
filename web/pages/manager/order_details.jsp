<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<meta charset="UTF-8">
<title>订单详情</title>
<jsp:include page="/WEB-INF/include/base.jsp"></jsp:include>
<style type="text/css">
	h1 {
		text-align: center;
		margin-top: 200px;
	}
</style>
</head>
<body>
	
	<div id="header">
			<jsp:include page="/WEB-INF/include/logo.jsp"></jsp:include>
			<span class="wel_word">订单详情</span>
			<jsp:include page="/WEB-INF/include/manager_head.jsp"></jsp:include>
	</div>
	
	<div id="main">
		
		<table>
			<tr>
				<td>日期</td>
				<td>封面</td>
				<td>标题</td>
				<td>作者</td>
				<td>数量</td>
				<td>价格</td>
			</tr>		

			<c:forEach items="${requestScope.list}" var="orderItem">
				<tr>
					<%--创建时间--%>
					<td style="width: 300px">${pageScope.orderItem.createTime}</td>
					<td>
						<img style="width: 80px;height: 80px" src="${pageScope.orderItem.imgPath}">
					</td>
					<td>${pageScope.orderItem.title}</td>
					<td>${pageScope.orderItem.author}</td>
					<td>${pageScope.orderItem.count}</td>
					<td>${pageScope.orderItem.amount}</td>

			</c:forEach>
		</table>
		
	
	</div>
	
	<div id="bottom">
		<span>
			宸星书城.Copyright &copy;2020
		</span>
	</div>
</body>
</html>