<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<meta charset="UTF-8">
<title>添加图书</title>
<jsp:include page="/WEB-INF/include/base.jsp"></jsp:include>
<style type="text/css">
	h1 {
		text-align: center;
		margin-top: 200px;
	}
	
	h1 a {
		color:red;
	}
	
	input {
		text-align: center;
	}
</style>
</head>
<body>
		<div id="header">
			<jsp:include page="/WEB-INF/include/logo.jsp"></jsp:include>
			<span class="wel_word">添加图书</span>
			<div><a href="javascript:history.go(-1);">&nbsp;取消</a></div>
			<jsp:include page="/WEB-INF/include/manager_head.jsp"></jsp:include>
		</div>
		
		<div id="main">
			<form action="BookManagerServlet?method=addBook" method="post">
				<table>
					<tr>
						<td>名称</td>
						<td>价格</td>
						<td>作者</td>
						<td>销量</td>
						<td>库存</td>
						<td colspan="2">操作</td>
					</tr>		
					<tr>
						<td><input name="title" type="text"/></td>
						<td><input name="price" type="text"/></td>
						<td><input name="author" type="text"/></td>
						<td><input name="sales" type="text"/></td>
						<td><input name="stock" type="text"/></td>
						<td><input type="submit" value="提交"/></td>
					</tr>	
				</table>
			</form>
		</div>
		<div id="bottom">
			<span>
				宸星书城.Copyright &copy;2020
			</span>
		</div>
</body>
</html>