<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<meta charset="UTF-8">
<title>图书管理</title>
<jsp:include page="/WEB-INF/include/base.jsp"></jsp:include>
	<script type="text/javascript">
		$(function ()
		{
			//实现删除提示功能
			$(".deleteA").click(function ()
			{
				if(!confirm("您确认删除吗？"))
				{
					return false;
				}
			});
			//实现分页跳转页面功能
			$("#pageBtn").click(function ()
			{
				var pageNo=$("#pn_input").val();
				location="BookManagerServlet?method=getPage&pageNo="+pageNo;
			});
		});
	</script>
</head>
<body>

	<div id="header">
		<jsp:include page="/WEB-INF/include/logo.jsp"></jsp:include>
		<span class="wel_word">图书管理系统</span>
		<jsp:include page="/WEB-INF/include/manager_head.jsp"></jsp:include>
	</div>
	
	<div id="main">
		<table>
			<tr>
				<td>名称</td>
				<td>价格</td>
				<td>作者</td>
				<td>销量</td>
				<td>库存</td>
				<td colspan="2">操作</td>
			</tr>		
<%--			<tr>--%>
<%--				<td>时间简史</td>--%>
<%--				<td>20.00</td>--%>
<%--				<td>霍金</td>--%>
<%--				<td>200</td>--%>
<%--				<td>400</td>--%>
<%--				<td><a href="book_edit.jsp">修改</a></td>--%>
<%--				<td><a href="#">删除</a></td>--%>
<%--			</tr>--%>
			<c:forEach items="${requestScope.page.list}" var="book">
				<tr>
					<td>${pageScope.book.title}</td>
					<td>${pageScope.book.price}</td>
					<td>${pageScope.book.author}</td>
					<td>${pageScope.book.sales}</td>
					<td>${pageScope.book.stock}</td>
					<td><a href="BookManagerServlet?method=toUpdate&id=${pageScope.book.id}">修改</a></td>
					<td><a class="deleteA" href="BookManagerServlet?method=deleteBook&id=${pageScope.book.id}">删除</a></td>
				</tr>
			</c:forEach>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><a href="pages/manager/book_edit.jsp">添加图书</a></td>
			</tr>	
		</table>
		<jsp:include page="/WEB-INF/include/page.jsp"></jsp:include>
	</div>
	<div id="bottom">
		<span>
			宸星书城.Copyright &copy;2020
		</span>
	</div>
</body>
</html>