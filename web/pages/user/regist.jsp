<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<meta charset="UTF-8">
<title>宸星会员注册页面</title>
<jsp:include page="/WEB-INF/include/base.jsp"></jsp:include>
<style type="text/css">
	.login_form{
		height:420px;
		margin-top: 25px;
	}
</style>
<script type="text/javascript">
	$(function(){
		/*//获取注册按钮并给它绑定单击响应函数
		$("#sub_btn").click(function(){
			//获取用户名、密码、确认密码、邮箱、验证码
			var userName = $("#username").val();
			var password = $("#password").val();
			var repwd = $("#repwd").val();
			var email = $("#email").val();
			var code = $("#code").val();
			//使用正则表达式对用户名、密码、邮箱进行验证
			var userReg = /^[a-zA-Z0-9_-]{3,16}$/;
			if(!userReg.test(userName)){
				alert("请输入3-16位的数字、字母、下划线或减号的用户名！");
				return false;
			};
			var pwdReg = /^[a-zA-Z0-9_-]{6,18}$/;
			if(!pwdReg.test(password)){
				alert("请输入6-18位的数字、字母、下划线或减号的密码！");
				return false;
			};
			if(repwd != password){
				alert("两次输入的密码不一致！");
				return false;
			}
			var emailReg = /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/;
			if(!emailReg.test(email)){
				alert("邮箱格式不正确！");
				return false;
			}
			if(code == ""){
				alert("验证码不能为空！");
				return false;
			}
		});*/

		$("#codeImg").click(function ()
		{
			$(this).prop("src","code.jpg?x="+Math.random());
		});

		$("#sub_btn").click(function ()
		{
			$.ajax({
				url:"${pageContext.request.contextPath}/UserServlet?method=regist_ajax",
				type:"POST",
				// data:{
				// 	username:$("[name='username']").val(),
				// 	password:$("[name='password']").val(),
				// 	code:$("[name='code']").val(),
				// },
				data:$("form").serialize(), //有form表单的情况下，serialize（序列化成数据拼接）
				dataType:"text",
				success:function (result)
				{
					if(result=="code"){
						$(".errorMsg").html("验证码错误");
						$("#codeImg").prop("src","code.jpg?x="+Math.random()); //注册失败，刷新验证码
					}
					else
					{
						if(result=="username"){
							$(".errorMsg").html("用户名已经被注册");
							$("#codeImg").prop("src","code.jpg?x="+Math.random()); //注册失败，刷新验证码
						}
						else if(result=="success"){
							//注册成功跳转到成功页面
							location="${pageContext.request.contextPath}/pages/user/regist_success.jsp";
						}
					}
				}
			});
		});
	});
</script>
</head>
<body>
		<div id="login_header">
			<jsp:include page="/WEB-INF/include/logo.jsp"></jsp:include>
		</div>
		
			<div class="login_banner">
			
				<div id="l_content">
					<span class="login_word">欢迎注册</span>
				</div>
				
				<div id="content">
					<div class="login_form">
						<div class="login_box">
							<div class="tit">
								<h1>注册宸星会员</h1>
								<span class="errorMsg">${requestScope.errorMsg}</span>
							</div>
							<div class="form">
								<form action="UserServlet?method=regist" method="post">
									<label>用户名称：</label>
									<input class="itxt" type="text" value="${param.username}" placeholder="请输入用户名" autocomplete="off" tabindex="1" name="username" id="username" />
									<br />
									<br />
									<label>用户密码：</label>
									<input class="itxt" type="password" placeholder="请输入密码" autocomplete="off" tabindex="1" name="password" id="password" />
									<br />
									<br />
									<label>确认密码：</label>
									<input class="itxt" type="password" placeholder="确认密码" autocomplete="off" tabindex="1" name="repwd" id="repwd" />
									<br />
									<br />
									<label>电子邮件：</label>
									<input class="itxt" type="text" value="${param.email}" placeholder="请输入邮箱地址" autocomplete="off" tabindex="1" name="email" id="email" />
									<br />
									<br />
									<label>验证码：</label>
									<input class="itxt" type="text" style="width: 150px;" name="code"/>
									<img id="codeImg" alt="" src="code.jpg" style="float: right; margin-right: 40px; width: 100px;height: 40px">
									<br />
									<br />
									<input type="button" value="注册" id="sub_btn" />
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		<div id="bottom">
			<span>
				宸星书城.Copyright &copy;2020
			</span>
		</div>
</body>
</html>