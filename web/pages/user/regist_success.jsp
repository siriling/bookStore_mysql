<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<meta charset="UTF-8">
<title>宸星会员注册页面</title>
<jsp:include page="/WEB-INF/include/base.jsp"></jsp:include>
<style type="text/css">
	h1 {
		text-align: center;
		margin-top: 200px;
	}
	
	h1 a {
		color:red;
	}
</style>
</head>
<body>
		<div id="header">
				<jsp:include page="/WEB-INF/include/logo.jsp"></jsp:include>
				<span class="wel_word">注册成功</span>
				<jsp:include page="/WEB-INF/include/client_head.jsp"></jsp:include>
		</div>
		
		<div id="main">
		
			<h1>注册成功! <a href="index.jsp">转到主页</a></h1>
	
		</div>
		
		<div id="bottom">
			<span>
				宸星书城.Copyright &copy;2020
			</span>
		</div>
</body>
</html>