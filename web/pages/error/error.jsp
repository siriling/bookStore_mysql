<%--
  Created by IntelliJ IDEA.
  User: Ling
  Date: 2020/10/31
  Time: 9:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>错误</title>
    <jsp:include page="/WEB-INF/include/base.jsp"></jsp:include>
</head>
<body>
<div id="header">
    <jsp:include page="/WEB-INF/include/logo.jsp"></jsp:include>
    <span class="wel_word">错误</span>
</div>
<div id="error">
    系统故障，请联系管理员。
    <br/><a href="">回到首页</a>
</div>
</body>
</html>
