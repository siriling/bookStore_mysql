<%--
  Created by IntelliJ IDEA.
  User: Ling
  Date: 2020/10/7
  Time: 16:20
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${empty sessionScope.user}">
    <div>
        <a href="pages/user/login.jsp">登录</a>
        <a href="pages/user/regist.jsp">注册</a>
        <a href="pages/cart/cart.jsp">购物车</a>
    </div>
</c:if>
<c:if test="${not empty sessionScope.user}">
    <div>
        <span>欢迎<span class="um_span">${sessionScope.user.username}</span>光临宸星书城</span>
        <a href="pages/cart/cart.jsp">购物车</a>
        <a href="OrderClientServlet?method=getMyOrder">我的订单</a>
        <a href="UserServlet?method=logout">注销</a>
        <a href="javascript:history.go(-1);">返回</a>
    </div>
</c:if>

