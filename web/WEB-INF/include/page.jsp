<%--
  Created by IntelliJ IDEA.
  User: Ling
  Date: 2020/10/16
  Time: 23:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="page_nav">

    <c:choose>
        <c:when test="${requestScope.page.totalPage<=5}">
            <c:set var="begin" value="1"></c:set>
            <c:set var="end" value="${requestScope.page.totalPage}"></c:set>
        </c:when>
        <c:when test="${requestScope.page.pageNo<3}">
            <c:set var="begin" value="1"></c:set>
            <c:set var="end" value="5"></c:set>
        </c:when>
        <c:otherwise>
            <c:set var="begin" value="${requestScope.page.pageNo-2}"></c:set>
            <c:set var="end" value="${requestScope.page.pageNo+2}"></c:set>
            <c:if test="${pageScope.end>requestScope.page.totalPage}">
                <c:set var="begin" value="${requestScope.page.totalPage-4}"></c:set>
                <c:set var="end" value="${requestScope.page.totalPage}"></c:set>
            </c:if>
        </c:otherwise>
    </c:choose>
    <c:forEach begin="${pageScope.begin}" end="${pageScope.end}" var="i">
        <c:if test="${pageScope.i==requestScope.page.pageNo}">
            <a style="color: cornflowerblue">【${pageScope.i}】</a>
        </c:if>
        <c:if test="${pageScope.i!=requestScope.page.pageNo}">
            <a href="${requestScope.page.path}&pageNo=${pageScope.i}">${pageScope.i}</a>
        </c:if>
    </c:forEach>

    <c:if test="${requestScope.page.pageNo!=1}">
        <a href="${requestScope.page.path}&pageNo=1">首页</a>
        <a href="${requestScope.page.path}&pageNo=${requestScope.page.pageNo-1}">上一页</a>
    </c:if>

    <c:if test="${requestScope.page.pageNo!=requestScope.page.totalPage}">
        <c:choose>
            <c:when test="${requestScope.page.totalCount==0}">
            </c:when>
            <c:otherwise>
                <a href="${requestScope.page.path}&pageNo=${requestScope.page.pageNo+1}">下一页</a>
                <a href="${requestScope.page.path}&pageNo=${requestScope.page.totalPage}">末页</a>

            </c:otherwise>
        </c:choose>
    </c:if>

    共${requestScope.page.totalPage}页，${requestScope.page.totalCount}条记录
    <c:choose>
        <c:when test="${requestScope.page.totalCount==0}">
            <input type="button" value="回到首页" id="pageBtn">
        </c:when>
        <c:otherwise>
            到第<input value="${requestScope.page.pageNo}" name="pn" id="pn_input"/>页
            <input type="button" value="确定" id="pageBtn">
        </c:otherwise>
    </c:choose>
</div>
