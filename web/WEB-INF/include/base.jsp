<%--
  Created by IntelliJ IDEA.
  User: Ling
  Date: 2020/10/7
  Time: 13:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<title>include</title>
<link rel="shortcut icon" href="static/img/favicon.ico"/>
<base href="<%=request.getScheme()%>://<%=request.getServerName()%>:<%=request.getServerPort()%><%=request.getContextPath()%>/">
<link type="text/css" rel="stylesheet" href="static/css/style.css" >
<script type="text/javascript" src="static/script/jquery-1.7.2.js"></script>

<%--协议：<%=request.getScheme()%><br>--%>
<%--ip：<%=request.getServerName()%><br>--%>
<%--port：<%=request.getServerPort()%><br>--%>
<%--工程名：<%=request.getContextPath()%><br>--%>