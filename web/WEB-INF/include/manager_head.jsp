<%--
  Created by IntelliJ IDEA.
  User: Ling
  Date: 2020/10/7
  Time: 16:22
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div>
    <a href="BookManagerServlet?method=getPage&pageNo=1">图书管理</a>
    <a href="OrderManagerServlet?method=getAllOrder">订单管理</a>
    <a href="UserServlet?method=logout">注销</a>
    <c:choose>
        <c:when test="${empty requestScope.page.list and empty requestScope.list}">
<%--            返回超链接不显示--%>
        </c:when>
        <c:otherwise>
            <a href="javascript:history.go(-1);">返回</a>
        </c:otherwise>
    </c:choose>
</div>