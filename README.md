# bookStore_mysql

## 介绍
JavaWeb入门项目：宸星书城，是一个简易的图书售卖网站。

## 软件架构

前端：jsp网页+jQuery+EL表达式+jstl+Ajax

后端：MySql+JDBC+Tomcat

## 安装教程

### V2.0之前的版本

##### 环境准备：

java8，idea，Tomcat9，MySql8

##### 项目准备：

1. 打开idea创建一个项目名为：**bookStore_mysql**的JavaWeb工程并配置好Tomcat
2. 使用git clone命令把项目下载到本地,并覆盖到在idea中创建的项目
3. 把项目目录下的.sql文件打开，在mysql建立所需要的表

##### 启动前配置：

1. 按照自己的数据库信息配置src/jdbc.properties

2. 打开web/WEB-INF/lib目录：

   把kaptcha-2.3.2.jar导入(add as Library...)为kaptcha

   把taglibs-standard-impl-1.2.5.jar和taglibs-standard-spec-1.2.5.jar导入为jstl

   把Tomcat的Servlet包复制到lib目录下，然后把servlet-api.jar导入为servlet

   把Mysql的JDBC包复制到lib目录下，然后把mysql-connector-java-8.0.22.jar(文件名不一定为此名)、commons-dbutils-1.6.jar和druid-1.1.10.jar导入为mysql+druid

### V2.0版本

##### 环境准备：

java11，idea，Tomcat10，MySql8

##### 项目准备：

1. 打开idea创建一个项目名为：**bookStore_mysql**的JavaWeb工程并配置好Tomcat
2. 使用git clone命令把项目下载到本地,并覆盖到在idea中创建的项目
3. 把项目目录下的.sql文件打开，在mysql建立所需要的表

##### 启动前配置：

1. 按照自己的数据库信息配置src/jdbc.properties

2. 打开web/WEB-INF/lib目录：

   把kaptcha-2.3.2.jar导入(add as Library...)为kaptcha

   把jakarta.servlet.jsp.jstl-3.0.0.jar和jakarta.servlet.jsp.jstl-api-3.0.0.jar导入为jstl

   把Tomcat的Servlet包复制到lib目录下，然后把servlet-api.jar导入为servlet

   把Mysql的JDBC包复制到lib目录下，然后把mysql-connector-java-8.0.22.jar(文件名不一定为此名)、commons-dbutils-1.6.jar和druid-1.1.10.jar导入为mysql+druid

## 使用说明

1.  项目都配置好以后点击idea中的启动
2.  书城里可注册用户
3.  书城里可增删改查图书
4.  书城里可购买图书和发货

## 项目展示

#### 主页

![image-20220718124700934](images/image-20220718124700934.png)

#### 登录页

#### ![image-20220718124839131](images/image-20220718124839131.png)注册页

#### ![image-20220718130842009](images/image-20220718130842009.png)购物车

#### ![image-20220718130934551](images/image-20220718130934551.png)后台管理

![image-20220718131022555](images/image-20220718131022555.png)

#### 图书管理

![image-20220718131058163](images/image-20220718131058163.png)

#### 订单管理

![image-20220718131235180](images/image-20220718131235180.png)

#### 订单详情

![image-20220718131159564](images/image-20220718131159564.png)

## 其他问题

如果要设置https反向代理，则需要修改base.jsp文件，将其中的base标签改为

```jsp
<base href="https://<%=request.getServerName()%>:81<%=request.getContextPath()%>/">
```

## 参与贡献

本项目由我一人完成，若转载请注明出处https://gitee.com/siriling/bookStore_mysql